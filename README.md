## Hyojun.guideline lite

`v0.1.0`

This is a simple bootstrap using Hyojun.guideline to create static (html output) websites with a front-end guideline.
For more information, visit the [wiki page](https://bitbucket.org/mcarneiro/hyojun.guideline-lite/wiki/Home).