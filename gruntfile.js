module.exports = function(grunt) {

	"use strict";

	var rjsModules = [],
		config,
		useRSync = grunt.file.exists("../.rsync-config");

	require("jit-grunt")(grunt, {
		scsslint: "grunt-scss-lint"
	});
	require("time-grunt")(grunt);

	grunt.file.recurse("assets/js/main/", function(abs, root, sub, file){
		if (file.match(/\.js$/i)) {
			rjsModules.push({"name": file.replace(/\.js/i, "")});
		}
	});

	config = {
		paths: {
			"templates": "./templates",
			"site_output": ".",
			"gl_output": "./guideline",
			"html_output": "/guideline",

			"layouts": "<%= paths.templates %>/layouts",
			"partials": "<%= paths.templates %>/partials",
			"includes": "<%= paths.templates %>/includes",
			"content": "<%= paths.templates %>/content",
			"data": "<%= paths.templates %>/data",

			"js_html_output": "/assets/js/dist",
			"sass_html_output": "/assets/css",

			rjs: {
				bower: "../../../bower_components"
			},
			rsync: {
				output: null
			}
		},
		concat: {},
		syncModules: [],
		requirejs: {
			"main": {
				"options": {
					"baseUrl": ".",
					"appDir": "assets/js/main",
					"dir": "assets/js/dist",
					"generateSourceMaps": true,
					"paths": {
						"mod": "../modules",
						"wrp": "../wrappers",
						"almond": "<%=paths.rjs.bower%>/almond/almond",
						"jquery": "<%=paths.rjs.bower%>/jquery/jquery",
						"hyojun.guideline": "<%= paths.rjs.bower%>/Hyojun.Guideline/assets/js/dist/guideline"
					},
					"optimize": "none",
					"modules": rjsModules,
					"onBuildRead": function(mod, path, content) {
						if (config.syncModules.indexOf(mod) > -1) {
							return "queue(function(){\n" + content + "\n});";
						}
						return content;
					}
				}
			}
		},
		uglify: {
			"main": {
				"options": {
					"sourceMap": true,
					"sourceMapIncludeSources": true,
					"sourceMapIn": function(source) {
						return source + ".map";
					},
					"preserveComments": false
				},
				"files": [{
					"expand": true,
					"cwd": "assets/js/dist",
					"src": ["**/*.js"],
					"dest": "assets/js/dist",
					"ext": ".js"
				}]
			}
		},
		sass: {
			"main": {
				"options": {
					"loadPath": "bower_components",
					"update": true,
					"bundleExec": true,
					"require": [
						"./bower_components/SASS-Base64/url64.rb"
					]
				},
				"files": [{
					"expand": true,
					"cwd": "assets/sass/output",
					"src": ["**/*.scss"],
					"dest": "assets/css",
					"ext": ".css"
				}]
			},
			"dist": {
				"options": {
					"style": "compressed",
					"loadPath": "bower_components",
					"sourcemap": "inline",
					"noCache": true,
					"bundleExec": true,
					"require": [
						"./bower_components/SASS-Base64/url64.rb"
					]
				},
				"files": [{
					"expand": true,
					"cwd": "assets/sass/output",
					"src": ["**/*.scss"],
					"dest": "assets/css",
					"ext": ".css"
				}]
			}
		},
		scsslint: {
			main: [
				"assets/sass/**/*.scss"
			],
			options: {
				bundleExec: true,
				colorizeOutput: true,
				config: ".scss-lint.yml"
			}
		},
		jshint: {
			options: grunt.file.readJSON(".jshintrc"),
			main: [
				"gruntfile.js",
				"assets/js/**/*.js",
				"!assets/js/dist/**/*.js"
			]
		},
		exec: {
			"npm": "npm install",
			"bower": "bower install -F",
			"bundle": "bundle install",
			"sync": [
				"rsync",
				"-rvuzW",
				"--delete",
				"--exclude-from=../.rsync-ignore",
				"../",
				"<%=paths.rsync.output%>"
			].join(" ")
		},
		watch: {
			requirejs: {
				files: [
					"assets/**/*.js",
					"!assets/js/dist/**/*.js"
				],
				tasks: ["requirejs"]
			},
			sass: {
				files: ["assets/sass/**/*.scss"],
				tasks: ["sass:main"]
			},
			render: {
				"files": [
					"./templates/**/*.mustache",
					"./templates/**/*.json"
				],
				"tasks": ["render-views"]
			}
		},
		"render-views": grunt.file.readJSON("./templates/views.json"),
		connect: {
			server: {
				options: {
					port: 8000,
					keepalive: true,
					base: "."
				}
			}
		}
	};

	// "private" tasks (TODO: see what can be done to not have this as tasks)
	grunt.registerMultiTask("render-views", "Render all views for a selected language", function () {
		var task = require("./bower_components/Hyojun.Guideline/generator/grunt/tasks/task-render-view");
		task.run(grunt, this);
	});
	grunt.registerTask("render-template", "Render target template", function (test_name) {
		var task = require("./bower_components/Hyojun.Guideline/generator/grunt/tasks/task-render-file");
		task.run(grunt, this);
	});

	if (useRSync) {
		config.paths.rsync = grunt.file.readJSON("../.rsync-config");
		config.watch.sync = {
			files: [
				"assets/css/**/*.css",
				"**/*.config",
				"assets/js/dist/**/*.js",
				"assets/img/**/*.*",
				"assets/fonts/**/*.*",
				"Views/**/*.*",
				"App_Code/**/*.*"
			],
			tasks: ["exec:sync"],
			options: {
				livereload: true
			}
		};
		grunt.registerTask("sync", ["exec:sync"]);
	} else {
		grunt.registerTask('sync', function() {
			grunt.log.writeln([
				"Nota:",
				" Arquivo '.rsync-config' não encontrado na raíz do projeto.",
				" Task 'sync' e 'watch:sync' não estará disponível. \n",
				"Para utilizar esta task, crie o arquivo com o JSON: ",
				"{ \"output\": \"/Volumes/DIRETORIO/DE/DESTINO\" }"
			].join(''));
		});
	}

	grunt.initConfig(config);

	grunt.registerTask("packages", ["exec:npm", "exec:bower", "exec:bundle"]);
	grunt.registerTask("lint", ["scsslint", "jshint"]);
	grunt.registerTask("js:dev", ["jshint", "requirejs"]);
	grunt.registerTask("css:dev", ["scsslint", "sass:main"]);
	grunt.registerTask("dev", ["css:dev", "js:dev"]);

	grunt.registerTask("js", ["js:dev", "uglify"]);
	grunt.registerTask("css", ["scsslint", "sass:dist"]);
	grunt.registerTask("default", ["render-views", "css", "js"]);
};
